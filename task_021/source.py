n, s = [int(i) for i in input().split()]
sm = sum(int(i) for i in input().split())
print((sm + s - 1) // s)
