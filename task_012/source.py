n, k = [int(i) for i in input().split()]
min_k = 10 ** 9 + 1
for c in input():
    min_k = min(k, min_k)
    k += ord('B') - ord(c)

print(max(0, -min_k))
