n = int(input())
a = [int(i) for i in input().split()]
print(sum(1 if a[i] > a[i + 1] else 0 for i in range(n - 1)))
