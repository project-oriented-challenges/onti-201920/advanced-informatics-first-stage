#include <bits/stdc++.h>

using namespace std;

typedef long long ll;

#define P pair
#define fi first
#define se second

#define V vector
typedef V<int> Vi;
typedef V<ll> Vll;

#define all(v) (v).begin(), (v).end()

#define forn(i, n) for (int (i) = 0; (i) < (n); (i)++)

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    cout.tie(nullptr);

    int n, m;
    cin >> n >> m;
    Vll leftX(n), downY(n), rightX(n), upY(n);
    forn(i, n) {
        cin >> leftX[i] >> downY[i] >> rightX[i] >> upY[i];
    }
    Vll x(m), y(m);
    forn(i, m) {
        cin >> x[i] >> y[i];
    }

    V<P<ll, int>> s(n * 2 + m);
    forn(i, n) {
        s[i * 2] = {leftX[i], -i - 1};
        s[i * 2 + 1] = {rightX[i], m + i};
    }
    forn(i, m) {
        s[n * 2 + i] = {x[i], i};
    }
    sort(all(s));

    Vi killed_by(n);
    fill(all(killed_by), INT_MAX);

    map<ll, int> mp;

    forn(i, n * 2 + m) {
        if (s[i].se < 0) {
            mp[downY[-s[i].se - 1]] = -s[i].se - 1;
        } else if (s[i].se >= m) {
            mp.erase(downY[s[i].se - m]);
        } else if (!mp.empty() && mp.upper_bound(y[s[i].se]) != mp.begin() && 
                    y[s[i].se] <= upY[(--mp.upper_bound(y[s[i].se]))->se]) {
            killed_by[(--mp.upper_bound(y[s[i].se]))->se] = 
                min(killed_by[(--mp.upper_bound(y[s[i].se]))->se], s[i].se);
        }
    }

    Vi ans(m);
    fill(all(ans), -1);
    forn(i, n) {
        if (killed_by[i] < INT_MAX) {
            ans[killed_by[i]] = i + 1;
        }
    }
    forn(i, m) {
        cout << ans[i] << ' ';
    }

    return 0;
}