n, m, tp = [int(i) for i in input().split()]
need = set(input().split())
lists = {}
rev_lists = {}

for i in range(n):
    s = input()
    lists.setdefault(s, [])
    rev_lists.setdefault(s, [])
    for t in input().split()[1:]:
        lists.setdefault(t, [])
        lists[t].append(s)
        rev_lists[s].append(t)

q = [""] * n
ql = qr = 0
for s in lists.keys():
    if len(rev_lists[s]) == 0:
        q[qr] = s
        qr += 1

cnt = {}
while ql < qr:
    s = q[ql]
    ql += 1
    for t in lists[s]:
        cnt.setdefault(t, 0)
        cnt[t] += 1
        if cnt[t] == len(rev_lists[t]):
            q[qr] = t
            qr += 1

for s in reversed(q):
    if s in need:
        for t in rev_lists[s]:
            need.add(t)

ans = []
for s in q:
    if s in need:
        ans.append(s)

print(len(ans))
if tp == 2:
    print(' '.join(map(str, ans)))
