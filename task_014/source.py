n, m = [int(i) for i in input().split()]

better = [[] for i in range(int(1e6) + 1)]

for i in range(m):
    film1, film2 = [int(i) for i in input().split()]
    better[film2].append(film1)

for i in range(n):
    film1, film2, film3 = [int(i) for i in input().split()]
    ok = len(better[film1]) == 0
    for f in better[film2]:
        if not f == film1:
            ok = False
            break
    for f in better[film3]:
        if (not f == film1) and (not f == film2):
            ok = False
            break
    print("honest" if ok else "liar")
