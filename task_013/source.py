n, ai, b, max = [int(i) for i in input().split()]

if b == 1:
    print(min(max // ai, n))
    exit(0)

# b > 1
l, s = 1, ai
while l <= n:
    if s > max:
        print(l - 1)
        exit(0)
    ai *= b
    s += ai
    l += 1

print(n)
