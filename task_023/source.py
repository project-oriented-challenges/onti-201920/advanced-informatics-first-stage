n = int(input())
a = [int(i) for i in input().split()]
a.sort()
sum_a = sum(a)

if sum_a % 5 != 0:
    print('No')
    exit(0)

cur, cnt = 0, 1
for i in a:
    cur += i
    if cur == sum_a // 5 * cnt:
        cnt += 1
        if cnt == 6:
            break

print('Yes' if cnt == 6 else 'No')