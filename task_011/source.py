c, x, y, z, m, s, f, e, t = [int(i) for i in input().split()]
print(min((c + (max(0, c - x) + z - 1) // z * y + s - 1) // s * m,
          ((e + 1) * f + s - 1) // s * m + t))
