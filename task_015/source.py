def pi(s):
    n = len(s)
    pi = [0] * n
    j = 0
    for i in range(1, n):
        while j > 0 and s[i] != s[j]:
            j = pi[j - 1]
        if s[i] == s[j]:
            j += 1
        pi[i] = j
    return pi


n = int(input())
s = input()

pi = pi(s[::-1] + '#' + s + s)
for i in range(n):
    if pi[n * 2 + i] == n:
        print(i)
        break
else:
    print(-1)
